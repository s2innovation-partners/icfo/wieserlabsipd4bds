import setuptools

setuptools.setup(
    name="tangods-wieserLabsIPD4B",
    version="1.0.0",
    description="Device server for Wieserkabs WL-IPD4B photo diode",
    author="S2innovation",
    author_email="contact@s2innovation.com",
    license="Proprietary",
    classifiers=[
        'License :: Other/Proprietary License'],
    packages=setuptools.find_packages(),
    install_requires=['statistics'],
    entry_points={
        'console_scripts': [
            "WieserLabsIPD4B=WieserLabsIPD4B.WieserLabsIPD4B:main",
        ]
    }

)

