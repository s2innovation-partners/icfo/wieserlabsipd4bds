import logging
import sys
from random import randint
from statistics import mean, stdev
from time import sleep

from tango.server import Device, attribute, device_property, DeviceMeta, command, run
from tango import DebugIt, DispLevel, AttrWriteType, DevState, Attr, DevDouble
from WieserLabsIPD4Blib import WieserLabsIPD4BController, WieserLabsIPD4BException

debug_it = DebugIt(show_args=True, show_kwargs=True, show_ret=True)


class WieserLabsIPD4B(Device):  # pylint: disable=too-many-instance-attributes
    __metaclass__ = DeviceMeta

    # ---------------------------------
    #   Properties
    # ---------------------------------

    ReadPeriodTau1 = device_property(
        dtype=int,
        default_value=1000,
        doc="Polling period for tau1 reading data in ms for the WL-IPD4B photodiode"
    )

    ReadPeriodTau2 = device_property(
        dtype=int,
        default_value=10000,
        doc="Polling period for tau2 reading data in ms for the WL-IPD4B photodiode"
    )

    SetPhotodiode = device_property(
        dtype=str,
        mandatory=True,
        doc="List of commands to set the photodiode"
    )

    PhotodiodesNos = device_property(
        dtype=int,
        default_value=3,
        doc="Number of photodiods connected to  WL-IPD4B."
    )

    Simulated = device_property(
        dtype=bool,
        default_value=False,
        doc="""Data generation mode. If True data are randomly generated.
         If False data are obtained from WL-IPD4B photodiode"""
    )

    Symlink = device_property(
        dtype="str",
        mandatory=True,
        doc="Symlink of the WL-IPD4B photodiode's device"
    )

    # ---------------------------------
    #   Global methods
    # ---------------------------------

    @debug_it
    def init_device(self):
        Device.init_device(self)

        self.set_state(DevState.ON)

        self.results = {}

        for diode in range(self.PhotodiodesNos):
            diode_attr = Attr("Diode%s" % diode, DevDouble, AttrWriteType.READ)
            diode_attr.set_polling_period(self.ReadPeriodTau1)
            self.add_attribute(diode_attr, self.get_mean_t1)

            diode_min_t2 = Attr("Diode%s_min_t2" % diode, DevDouble, AttrWriteType.READ)
            diode_min_t2.set_polling_period(self.ReadPeriodTau2)
            self.add_attribute(diode_min_t2, self.get_min_t2)

            diode_max_t2 = Attr("Diode%s_max_t2" % diode, DevDouble, AttrWriteType.READ)
            diode_max_t2.set_polling_period(self.ReadPeriodTau2)
            self.add_attribute(diode_max_t2, self.get_max_t2)

            diode_mean__std_t2 = Attr("Diode%s_mean_std_t2" % diode, DevDouble, AttrWriteType.READ)
            diode_mean__std_t2.set_polling_period(self.ReadPeriodTau2)
            self.add_attribute(diode_mean__std_t2, self.get_mean_std_t2)

            diode_mean_std_t2 = Attr("Diode%s_mean_plus_std_t2" % diode, DevDouble, AttrWriteType.READ)  # pylint: disable=line-too-long
            diode_mean_std_t2.set_polling_period(self.ReadPeriodTau2)
            self.add_attribute(diode_mean_std_t2, self.get_mean_plus_std_t2)

            self.results[diode_attr.get_name()] = {
                'mean_t1': 0,
                'min_t2': 0,
                'mean-std_t2': 0,
                'mean+std_t2': 0,
                'max_t2': 0,
                'normalization': 1
            }

        if not self.Simulated:
            try:
                self.diode = WieserLabsIPD4BController(self.Symlink, self.PhotodiodesNos, self.SetPhotodiode.split(','))  # pylint: disable=line-too-long
                sleep(2)
            except WieserLabsIPD4BException:
                self.set_state(DevState.FAULT)
                self.error_stream(str(sys.exc_info()[1]))
                logging.error(str(sys.exc_info()[1]))
                raise sys.exc_info()[1]

    @debug_it
    def delete_device(self):
        self.diode.disconnect()

    @debug_it
    def get_mean_t1(self, attr):
        attr.set_value(self.results[attr.get_name()]['mean_t1'] / self.results[attr.get_name()]['normalization'])  # pylint: disable=line-too-long

    @debug_it
    def get_min_t2(self, attr):
        attr.set_value(self.results[attr.get_name().split("_")[0]]['min_t2'] / self.results[attr.get_name().split("_")[0]]['normalization'])  # pylint: disable=line-too-long

    @debug_it
    def get_max_t2(self, attr):
        attr.set_value(self.results[attr.get_name().split("_")[0]]['max_t2'] / self.results[attr.get_name().split("_")[0]]['normalization'])  # pylint: disable=line-too-long

    @debug_it
    def get_mean_std_t2(self, attr):
        attr.set_value(self.results[attr.get_name().split("_")[0]]['mean-std_t2'] / self.results[attr.get_name().split("_")[0]]['normalization'])  # pylint: disable=line-too-long

    @debug_it
    def get_mean_plus_std_t2(self, attr):
        attr.set_value(self.results[attr.get_name().split("_")[0]]['mean+std_t2'] / self.results[attr.get_name().split("_")[0]]['normalization'])  # pylint: disable=line-too-long

    @command
    def save_normalization_diode_0(self):
        """
        Save normalization for diode 0
        """
        self.results["Diode0"]['normalization'] = (self.results["Diode0"]['mean-std_t2'] + self.results["Diode0"]['mean+std_t2']) * 0.5  # pylint: disable=line-too-long

    @command
    def save_normalization_diode_1(self):
        """
        Save normalization for diode 1
        """
        self.results["Diode1"]['normalization'] = (self.results["Diode1"]['mean-std_t2'] + self.results["Diode1"]['mean+std_t2']) * 0.5  # pylint: disable=line-too-long

    @command
    def save_normalization_diode_2(self):
        """
        Save normalization for diode 2
        """
        self.results["Diode2"]['normalization'] = (self.results["Diode2"]['mean-std_t2'] + self.results["Diode2"]['mean+std_t2']) * 0.5  # pylint: disable=line-too-long

    @command
    def save_normalization_diode_3(self):
        """
        Save normalization for diode 3
        """
        self.results["Diode3"]['normalization'] = (self.results["Diode3"]['mean-std_t2'] + self.results["Diode3"]['mean+std_t2']) * 0.5  # pylint: disable=line-too-long

    def get_simulated_data(self, photodiods_range):
        for idx in range(photodiods_range):
            self.results[f"Diode{idx}"]['mean_t1'] = randint(0, 1048576)

    @command(
        polling_period=ReadPeriodTau1.default_value
    )
    @debug_it
    def get_results_tau1(self):
        """
        obtain results for tau1
        """
        if self.Simulated:
            self.get_simulated_data(self.PhotodiodesNos)
        else:
            for idx, value in enumerate(self.diode.get_values_primary()):
                self.results[f"Diode{idx}"]['mean_t1'] = value

    @command(
        polling_period=ReadPeriodTau2.default_value
    )
    @debug_it
    def get_results_tau2(self):
        """
        obtain results for tau2
        """
        if self.Simulated:
            for idx in range(self.PhotodiodesNos):
                self.results[f"Diode{idx}"]['min_t2'] = randint(0, 1048576)
                self.results[f"Diode{idx}"]['mean-std_t2'] = randint(0, 1048576)
                self.results[f"Diode{idx}"]['mean+std_t2'] = randint(0, 1048576)
                self.results[f"Diode{idx}"]['max_t2'] = randint(0, 1048576)
        else:
            for idx, value in enumerate(self.diode.get_values_t2()):
                self.results[f"Diode{idx}"]['min_t2'], self.results[f"Diode{idx}"]['mean-std_t2'], self.results[f"Diode{idx}"]['mean+std_t2'], self.results[f"Diode{idx}"]['max_t2'] = value  # pylint: disable=line-too-long


def main(args=None, **kwargs):
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.DEBUG)
    return run((WieserLabsIPD4B,), args=args, **kwargs)


if __name__ == '__main__':
    main()
